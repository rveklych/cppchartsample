﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Runtime.InteropServices;

namespace CppChartSample
{
    public partial class ChartForm : Form
    {
        private const double XMin = -1;
        private const double XMax = 1;

        /// <summary>
        /// Кількість інтервалів для побудови графіка.
        /// </summary>
        private const int ChartStepCount = 256;

        /// <summary>
        /// Кількість вузлів інтерполяції;
        /// для коректного відображення повинна бути менша, ніж ChartStepCount;
        /// в прикладі не використовується.
        /// </summary>
        private const int InterpolationNodeCount = 10;

        public ChartForm()
        {
            InitializeComponent();
        }

        private void ChartForm_Load(object sender, EventArgs e)
        {
            // малюємо графік при завантаженні форми
            Plot();
        }

        [DllImport("Interpolation.dll", EntryPoint = "Initialize", CallingConvention = CallingConvention.Cdecl)]
        public static extern double Initialize(double left, double right);


        [DllImport("Interpolation.dll", EntryPoint = "Lagrange", CallingConvention = CallingConvention.Cdecl)]
        public static extern double Lagrange(double x);

        [DllImport("Interpolation.dll", EntryPoint = "Spline", CallingConvention = CallingConvention.Cdecl)]
        public static extern double Spline(double x);

        private void Plot()
        {
            // за замовчуванням Chart створюється з однією послідовністю
            // але у вікні Properties для Chart можна змінювати поле Series та додавати нові
            var lagrangeSeries = chart.Series[0];

            // послідовності даних можна створювати і безпосередньо
            var splineSeries = new Series()
            {
                Name = "Spline",
                ChartType = SeriesChartType.FastLine,
                Color = Color.Red
            };
            chart.Series.Add(splineSeries);

            double XSpan = XMax - XMin;
            // крок для зображення функції
            double chartStep = XSpan / ChartStepCount;

            Initialize(XMin, XMax);

            // крок для побудови інтерполяції по рівновіддалених вузлах
            double h = XSpan / (InterpolationNodeCount - 1);

            for (double x = XMin; x < XMax; x += chartStep)
            {
                lagrangeSeries.Points.AddXY(x, Lagrange(x));
                splineSeries.Points.AddXY(x, Spline(x));
            }
        }

    }
}
