// Interpolation.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <cmath>

double xMin, xMax;

extern "C" __declspec(dllexport) void __cdecl Initialize(double left, double right)
{
	xMin = left;
	xMax = right;
}

extern "C" __declspec(dllexport) double __cdecl Lagrange(double x)
{
	return sin(x - xMin);
}

extern "C" __declspec(dllexport) double __cdecl Spline(double x)
{
	return cos(exp(x + xMax));
}